


module sec.forms {


    export interface ISecButtonController extends ng.IController {
        onSubmit(): void;
        updateValidation(valid: boolean): void;
    }



    class SecButtonController implements ISecButtonController {


        private FormCtrl: ISecFormController;
        private ButtonCtrl: ISecButtonController
        validForm: boolean;
        secSubmit: Function;

        $onInit() {
            this.FormCtrl.setButtonCtrl(this);
        }

        updateValidation(valid: boolean): void {
            this.validForm = valid;
        }

        onSubmit(): void {
            if (this.validForm) {
                this.secSubmit({ valid: this.validForm });
            }
            else {
                this.FormCtrl.enableErrors();
            }
        }
    }


    class SecButtonComponent {

        static instance: ng.IComponentController = {
            require: {
                FormCtrl: '^secForm',
            },
            bindings: {
                secValue: '@',
                secSubmit: '&',
            },
            controllerAs: 'buttonCtrl',
            template: `
                <button
                    data-ng-class="{ 'invalid': !buttonCtrl.validForm }"
                    data-ng-click="buttonCtrl.onSubmit()" >
                    {{buttonCtrl.secValue}}
                </button>
                `,
            controller: SecButtonController
        };

    }

    angular.module('sec.forms').component('secButton', SecButtonComponent.instance)

}