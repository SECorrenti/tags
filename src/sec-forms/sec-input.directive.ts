









module sec.forms {


    SecInputDirective.$inject = ['$templateCache'];

    function SecInputDirective($TC: ng.ITemplateCacheService): ng.IDirective {

        var directive = <ng.IDirective> {
            scope : true,
            restrict : 'E',
            controller : SecInputController,
            controllerAs : 'inputCtrl',
            template : $TC.get('src/sec-forms/sec-input.directive.html'),
            bindToController : {
                inputValue: '=',
                inputText: '@',
            },
            require : {
                FormCtrl: '^secForm',
                InputCtrl: 'secInput',
            },
            link: link,
        };

        function link(s, e, a, controllers: IPostControllers): void {
            controllers.InputCtrl.setFormCtrl(controllers.FormCtrl);
            controllers.FormCtrl.setInputCtrl(controllers.InputCtrl);
        }

        return directive;
    }





    interface IPostControllers {
        FormCtrl: ISecFormController,
        InputCtrl: ISecInputController
    }


    export interface ISecInputController {
        pushValidation(inputValidation: ng.IController): void;
        isValid(applyError?: boolean): boolean;
        setFormCtrl(formCtrl: ISecFormController): void;
        enableErrors(applyError?: boolean): boolean;
    }


    export interface ISecInputControllerCalls extends ng.IController {
        isValid(value: string): string;
    }


    class SecInputController implements ISecInputController {

        private inputValidations: ISecInputControllerCalls[] = [];


        inputValue: string = '';
        dirty: boolean;
        error: string;
        formCtrl: ISecFormController;

        setFormCtrl(formCtrl: ISecFormController): void {
            this.formCtrl = formCtrl;
        }

        pushValidation(inputValidation: ISecInputControllerCalls): void {
            this.inputValidations.push(inputValidation);
        }

        enableErrors(applyError: boolean = true): boolean {
            this.dirty = true;
            return this.isValid(applyError);
        }

        isValid(applyError: boolean = true): boolean {

            for (let index = 0; index < this.inputValidations.length; index++) {
                const validations = this.inputValidations[index];
                const error = validations.isValid(this.inputValue);
                if (applyError) {
                    this.error = error;
                }
                if (error) {
                    return false;
                }
            }
            return true;
        }

        onKeyup($event): void {
            this.isValid(false);
            this.formCtrl.checkSubmitBtn(false);
        }

        onBlur($event): void {
            this.dirty = true;
            if (this.isValid()) {
                this.formCtrl.checkSubmitBtn();
            }
        }

    }


    angular
        .module('sec.forms')
        .directive('secInput', SecInputDirective);
}





