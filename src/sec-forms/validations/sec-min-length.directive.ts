

module sec.forms.validation {
    
        interface ISecMinLengthController extends ng.IController {
            setMinLength(minLength: string): void;
        }
    
        class SecMinLengthController implements ISecMinLengthController, ISecInputControllerCalls {

            private error: string;            
            private minLength:number;
            
            setMinLength(minLength: string): void {
                this.minLength = parseInt(minLength);
            }

            isValid(value: string): any {
                this.error = value.length < this.minLength ? 
                'Use a minimum length of ' + this.minLength + 'character'  : null; 
                return this.error;    
            }

        }
    
        interface IPreControllers {
            InputCtrl: ISecInputController,
            validatorCtrl: ISecMinLengthController
        }
    
        class SecMinLengthDirective implements ng.IDirective {
            restrict = 'A';
            priority = 980;
            constrollerAs = 'secMinLength';
            controller = SecMinLengthController;
            require = {
                InputCtrl: 'secInput',
                validatorCtrl: 'secMinLength'
            };
    
            static newInstance(): ng.IDirective {
                return new SecMinLengthDirective;
            }
    
            link: ng.IDirectivePrePost = {
                pre: (
                    s: ng.IScope,
                    e: ng.IAugmentedJQuery,
                    attributes: ng.IAttributes,
                    controllers: IPreControllers
                ): void => {
                    controllers.validatorCtrl.setMinLength(attributes.secMinLength)
                    controllers.InputCtrl.pushValidation(controllers.validatorCtrl);
                }
            }
        }
    
        angular.module('sec.forms').directive('secMinLength', SecMinLengthDirective.newInstance)
    
    }