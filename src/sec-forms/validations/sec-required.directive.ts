


module sec.forms.validations {

    class SecRequiredController implements ISecInputControllerCalls {
        
        isValid(value: string): string {
            return value.length == 0 ? 'This field is required' : null;
        }
    }

    interface IPreControllers {
        InputCtrl: ISecInputController,
        validatorCtrl: ISecInputControllerCalls
    }

    class SecRequiredDirective implements ng.IDirective {

        restrict = 'A';
        priority = 990;
        constrollerAs = 'secRequired';
        controller = SecRequiredController;
        require = {
            InputCtrl: 'secInput',
            validatorCtrl: 'secRequired'
        };

        static newInstance(): ng.IDirective {
            return new SecRequiredDirective;
        }

        link: ng.IDirectivePrePost = {
            pre: (
                s: ng.IScope,
                e: ng.IAugmentedJQuery,
                a: ng.IAttributes,
                controllers: IPreControllers
            ): void => {
                controllers.InputCtrl.pushValidation(controllers.validatorCtrl);
            }
        }
    }


    angular.module('sec.forms').directive('secRequired', SecRequiredDirective.newInstance);

}