

module sec.forms.validation {

    class SecEmailController implements ISecInputControllerCalls {

        private readonly emailPattern = '^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,3})$';
     
        isValid(value: string): string {
            return !(new RegExp(this.emailPattern).test(value)) && (value).length != 0 ? 'Email no formatted correctly' : null; 
        }
    }

    interface IPreControllers {
        InputCtrl: ISecInputController,
        validatorCtrl: ISecInputControllerCalls
    }
    
    class SecEmailDirective implements ng.IDirective {

        restrict = 'A';
        priority = 980;
        constrollerAs = 'secEmail';
        controller = SecEmailController;
        require = {
            InputCtrl: 'secInput',
            validatorCtrl: 'secEmail'
        };

        static newInstance() : ng.IDirective {
            return new SecEmailDirective;
        }

        link: ng.IDirectivePrePost = {
            pre: (
                s: ng.IScope,
                e: ng.IAugmentedJQuery,
                a: ng.IAttributes,
                controllers: IPreControllers
            ): void => {
                controllers.InputCtrl.pushValidation(controllers.validatorCtrl);
            }
        }
    }

    angular.module('sec.forms').directive('secEmail', SecEmailDirective.newInstance)

}