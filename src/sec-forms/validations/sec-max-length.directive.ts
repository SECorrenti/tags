

module sec.forms.validation {

    export interface ISecMaxLengthController extends ng.IController {
        setMaxLength(minLength: string): void;        
    }

    class SecMaxLengthController implements ISecMaxLengthController, ISecInputControllerCalls {

        private maxLength;
            
        setMaxLength(maxLength: string): void {
            this.maxLength = parseInt(maxLength);
        }

        isValid(value: string): string {
            return value.length > this.maxLength ? 
            'Use length of ' + this.maxLength + 'character maximum' : null; 
        }
        
    }

    interface IPreControllers {
        InputCtrl: ISecInputController,
        validatorCtrl: ISecMaxLengthController
    }

    class SecMaxLengthDirective implements ng.IDirective {
        
        restrict = 'A';
        priority = 980;
        constrollerAs = 'secMaxLength';
        controller = SecMaxLengthController;
        require = {
            InputCtrl: 'secInput',
            validatorCtrl: 'secMaxLength'
        };

        static newInstance(): ng.IDirective {
            return new SecMaxLengthDirective;
        }

        link: ng.IDirectivePrePost = {
            pre: (
                s: ng.IScope,
                e: ng.IAugmentedJQuery,
                attributes: ng.IAttributes,
                controllers: IPreControllers
            ): void => {
                controllers.validatorCtrl.setMaxLength(attributes.secMaxLength)
                controllers.InputCtrl.pushValidation(controllers.validatorCtrl);
            }
        }
    }

    angular.module('sec.forms').directive('secMaxLength', SecMaxLengthDirective.newInstance)

}