

module sec.forms {
    
        'use strict';
    
    

        /* DIRECTIVE */
    
        class SecFormDirective implements ng.IDirective {
    
            restrict = 'E';
            controller = SecFormController;
            controllerAs = 'secForm';
            scope = true;
            

            static newInstance(): ng.IDirective {
                const ctrl = new SecFormDirective;
                return ctrl;
            }
    
            link(
                s: ng.IScope,
                element: ng.IAugmentedJQuery,
            ): void {
                element.addClass('sec-forms')
            }
    
        }
    
       
        /* INTERFACES */
        
        export interface ISecFormController {
            checkForm(applyError:boolean): boolean;
            checkSubmitBtn(applyError?:boolean): void;
            setInputCtrl(inputCtrl: ISecInputController): void;
            setButtonCtrl(buttonCtrl: ISecButtonController): void;
            enableErrors(): void;
        }

    
     
    
        /* CONTROLLERS */
    
        class SecFormController implements ISecFormController {
            
            inputCtrls:  ISecInputController[] = [];
            buttonsCtrls:  ISecButtonController[] = [];

            validationStatus: Function;

            setInputCtrl(inputCtrl: ISecInputController): void {
                this.inputCtrls.push(inputCtrl);
            }

            setButtonCtrl(buttonCtrl: ISecButtonController): void {
                this.buttonsCtrls.push(buttonCtrl);
            }

            checkForm(applyError:boolean = true): boolean {
                for (let index = 0; index < this.inputCtrls.length; index++) {
                    const ctrl = this.inputCtrls[index];
                    var valid = ctrl.isValid(applyError)
                    if(!valid){
                        return false;
                    }
                }
                return true;
            }

            checkSubmitBtn(applyError:boolean = true): void {
                var valid = this.checkForm(applyError);
                this.buttonsCtrls.forEach((btn: ISecButtonController)=>{
                    btn.updateValidation(valid);
                })
            }

            enableErrors(): void {
                this.inputCtrls.forEach((input: ISecInputController)=>{
                    input.enableErrors();
                })
            }
            
            $postLink?(): void {
                console.log('form $postLink?()');
                this.checkSubmitBtn(false);
            }

            
        }
    
    
    
        angular.module('sec.forms').directive('secForm', SecFormDirective.newInstance)
    
    }
    
    
    
    
    