


module sec.ripple {


    
    SecRippleDirective.$inject = ['$timeout'];

    function SecRippleDirective($timeout: ng.ITimeoutService): ng.IDirective {

        var directive = <ng.IDirective> {
            restrict : 'A',
            link: link,
        };

        function link(
            scope: ng.IScope,
            element: JQLite,
        ) {
            element[0].style['position'] = 'relative';
            element[0].style['overflow'] = 'hidden';
            element.on('mousedown touchstart', (event: JQueryEventObject) => {

                const max = Math.max(element[0].offsetWidth, element[0].offsetHeight) * 1.5;

                const circle = $('<div></div>');
                circle.css({ width: max + 'px', height: max + 'px',
                    left: event.clientX - element[0].offsetLeft + $(document).scrollLeft() - (max / 2) + 'px', 
                    top: event.clientY - element[0].offsetTop + $(document).scrollTop() - (max / 2) + 'px',
                });
                circle.addClass('sec-ripple');

                element.append(circle);

                $timeout((circle: HTMLElement) => {
                    circle.remove();
                }, 2000, false, circle);

            });
        
        }

        return directive;
    }

    angular.module('sec.ripple').directive('secRipple', SecRippleDirective);

}












