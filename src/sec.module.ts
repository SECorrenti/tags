

declare const angular: any;

((): void => {

    angular.module('sec', [
        'sec.forms',
        'sec.ripple',
        'ui.router'
    ]);

})();

