
((): void => {
    'use strict';

    angular
        .module('sec')
        .config(config);

    config.$inject = [
        '$stateProvider',
        '$urlRouterProvider'
    ];
    function config(
        $stateProvider: any,
        $urlRouterProvider: any): void {
            
        $stateProvider.state({
            name: 'main',
            url: '/main',
            component: 'mainComponent'
        });

        $stateProvider.state({
            name: 'socket',
            url: '/socket',
            component: 'socketComponent'
        });

        $stateProvider.state({
            name: 'home',
            url: '/home',
            component: 'homeComponent'
        });

        $stateProvider.state({
            name: 'about',
            url: '/about',
            component: 'aboutComponent'
        });

        $stateProvider.state({
            name: 'contact',
            url: '/contact',
            component: 'contactComponent'
        });

        $stateProvider.state({
            name: 'galery',
            url: '/galery',
            component: 'galeryComponent'
        });

        $stateProvider.state({
            name: 'reviews',
            url: '/reviews',
            component: 'reviewsComponent'
        });

        $urlRouterProvider.otherwise('/main');
    }
})();