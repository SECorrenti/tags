
module sec {



    interface IGaleryController {
        user: User;
        validationStatus(valid: boolean): void;
    }



    class GaleryController implements IGaleryController {

        user: User;
        private counter = 0;

        constructor(){
            this.user = new User('Sergio', 'Correnti', 'SECorrenti', 'secorrentigmail.com', '123', 34);
        }

        validationStatus(valid: boolean): void {
            console.log('checkSubmitBtn ' + valid);
            console.log(this.user);
        }


    }


    class GaleryComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/galery/galery.component.html')],
            controller: GaleryController, 
            controllerAs: 'galeryCtrl'

        }

    }


    angular.module('sec').component('galeryComponent', GaleryComponent.instance);

}