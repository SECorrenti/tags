
module sec {



    interface IContactController {
        user: User;
        validationStatus(valid: boolean): void;
    }



    class ContactController implements IContactController {

        user: User;
        private counter = 0;

        constructor(){
            this.user = new User('Sergio', 'Correnti', 'SECorrenti', 'secorrentigmail.com', '123', 34);
        }

        validationStatus(valid: boolean): void {
        }


    }


    class ContactComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/contact/contact.component.html')],
            controller: ContactController, 
            controllerAs: 'contactCtrl'

        }

    }


    angular.module('sec').component('contactComponent', ContactComponent.instance);

}