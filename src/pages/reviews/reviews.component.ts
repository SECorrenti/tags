
module sec {



    interface IReviewsController {
        user: User;
        validationStatus(valid: boolean): void;
    }



    class ReviewsController implements IReviewsController {

        user: User;
        private counter = 0;

        constructor(){
            this.user = new User('Sergio', 'Correnti', 'SECorrenti', 'secorrentigmail.com', '123', 34);
        }

        validationStatus(valid: boolean): void {
        }


    }


    class ReviewsComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/reviews/reviews.component.html')],
            controller: ReviewsController, 
            controllerAs: 'reviewsCtrl'

        }

    }


    angular.module('sec').component('reviewsComponent', ReviewsComponent.instance);

}