
module sec {



    interface IHomeController {
        user: User;
        validationStatus(valid: boolean): void;
    }



    class HomeController implements IHomeController {

        user: User;
        private counter = 0;

        constructor(){
            this.user = new User('Sergio', 'Correnti', 'SECorrenti', 'secorrentigmail.com', '123', 34);
        }

        validationStatus(valid: boolean): void {
        }


    }

    
    class HomeComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/home/home.component.html')],
            controller: HomeController, 
            controllerAs: 'homeCtrl'

        }

    }


    angular.module('sec').component('homeComponent', HomeComponent.instance);

}