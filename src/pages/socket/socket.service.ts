

module sec {

    export interface ISocketService {
        on(eventName:string, callback:Function): void;
        emit(eventName:string, data:any, callback: Function);
        socket: SocketIOClient.Socket
    }

    class SocketService implements ISocketService {
        static $inject = ['$rootScope'];
        socket = io.connect('http://localhost:8080/');
        constructor(private $rootScope: ng.IRootScopeService) {

        }

        on(eventName:string, callback:Function): void{
            this.socket.on(eventName, (args)=>{
                this.$rootScope.$apply(()=>{
                    callback.apply(this.socket, args);
                })
            });
        }
        

        emit(eventName:string, data:any, callback: Function): void{
            this.socket.emit(eventName, data, (args)=>{
                this.$rootScope.$apply(()=>{
                    if(callback){
                        callback.apply(this.socket, args);
                    }
                })
            })
        }
    }


    angular.module('sec').service('socketService', SocketService);

}