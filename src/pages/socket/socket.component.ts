namespace sec {
  interface ISocketController extends ng.IController {
    nickname: string;
    users: Array<any>;
    join(): void;
  }

  class SocketController implements ISocketController {
    nickname = "Effi";
    message = "Hola mundo";
    users = [];
    messages = [];
    userAdded = false;
    static $inject = ["socketService", "$scope"];

    constructor(private socketService: sec.ISocketService, $scope: ng.IScope) {
      socketService.socket.on("all-users", data => {
        $scope.$applyAsync(() => {
          this.users = data.filter(user => user.nickname !== this.nickname);
        });
      });
      socketService.socket.on("message-received", data => {
        $scope.$applyAsync(() => this.messages.push(data));
      });
    }

    $onInit(): void {
      this.socketService.socket.emit("get-users");
    }

    join(): void {
      if (this.nickname.replace(" ", "").length === 0) {
        return alert("no empty fields please, enter a username");
      }
      this.userAdded = true;
      this.socketService.emit(
        "join",
        {
          nickname: this.nickname
        },
        _ => {
          console.log(_);
        }
      );
    }

    sendMessage(): void {
      const newMessage = {
        text: this.message,
        from: this.nickname
      };
      this.socketService.socket.emit("send-message", newMessage);
      this.message = "";
      this.messages.push(newMessage);
    }
  }

  class SocketComponent {
    static instance: ng.IComponentOptions = {
      template: [
        "$templateCache",
        $TC => $TC.get("src/pages/socket/socket.component.html")
      ],
      controller: SocketController,
      controllerAs: "socketCtrl"
    };
  }

  angular.module("sec").component("socketComponent", SocketComponent.instance);
}
