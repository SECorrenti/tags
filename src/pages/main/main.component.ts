
module sec {



    interface IMainController {
        user: User;
        validationStatus(valid: boolean): void;
    }



    class MainController implements IMainController {

        user: User;
        private counter = 0;

        constructor(){
            this.user = new User('Sergio', 'Correnti', 'SECorrenti', 'secorrentigmail.com', '123', 34);
        }

        validationStatus(valid: boolean): void {
        }


    }


    class MainComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/main/main.component.html')],
            controller: MainController, 
            controllerAs: 'mainCtrl'

        }

    }


    angular.module('sec').component('mainComponent', MainComponent.instance);

}