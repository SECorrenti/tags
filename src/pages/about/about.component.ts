
module sec {



    interface IAboutController {
        user: User;
        validationStatus(valid: boolean): void;
    }



    class AboutController implements IAboutController {

        user: User;
        private counter = 0;

        constructor(){
            this.user = new User('Sergio', 'Correnti', 'SECorrenti', 'secorrentigmail.com', '123', 34);
        }

        validationStatus(valid: boolean): void {
        }


    }


    class AboutComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/about/about.component.html')],
            controller: AboutController, 
            controllerAs: 'aboutCtrl'

        }

    }


    angular.module('sec').component('aboutComponent', AboutComponent.instance);

}