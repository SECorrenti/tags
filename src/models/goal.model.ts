

module sec {


    interface IGoal {
        created: Date;
        value: string,
        position: number
    }


    export class Goal implements IGoal {
        
        created: Date;
        constructor(
            public value: string,
            public position: number
        ) {
            this.created = new Date();
        }
    }

    
}

