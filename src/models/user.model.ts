

module sec {
    

    interface IUser {
        firstName:string;
        lastName:string;
        userName:string;
        email:string;
        password:string;
        age:number;
        goals: Goal[];
    }



   export class User implements IUser {
        goals = [];
        constructor(
            public firstName: string,
            public lastName: string,
            public userName: string,
            public email: string,
            public password: string,
            public age: number
        ) { }
    }

}

