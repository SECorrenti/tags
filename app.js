var express = require('express');
var path = require('path');
var app = express('express');
var server = require('http').Server(app);
var io = require('socket.io')(server);

var port = 8080;
var users = [];

app.use(express.static(path.join(__dirname, '')))


io.on('connection', function(socket){

    console.log('New connection made');

    // OnInit: set all users on init
    socket.on('get-users', function(){
        socket.emit('all-users', users);
    });


    // Add a new user
    socket.on('join', function(data){
        socket.nickname = data.nickname;
        var socketObj = {
            nickname: socket.nickname,
            socketId: socket.id, 
        }
        users.push(socketObj);
        io.emit('all-users', users);
    });


    //Send to the other users (Skip my socket)
    socket.on('send-message', function(data){
        socket.broadcast.emit('message-received', data);
    })


    //Send to specipic users by user id
    socket.on('send-like', function(data){
        socket.broadcast.to(data.id).emit('user-liked', data);
    })


    // disconnect a user
    socket.on('disconnect', function(){
        users = users.filter(function(user) { 
            return user.nickname !== socket.nickname
        });
        io.emit('all-users', users);
    })


    // Create a private room.
    var private_room_name = 'private-room-name';
    socket.on('join-private', function(){
        socket.join(private_room_name);
    });


    // Send message to private chat.
    socket.on('message-to-private-chat', function(data){
        socket.broadcast.to(private_room_name).emit('show-message', data.message)
    });

});

server.listen(port, function(){
    console.log('Listen to port ' + port);
})















