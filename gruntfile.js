
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json"),

        ts: {
            options: {
                rootDir: "src",
            },
            main: {
                src: ["src/*.ts", "src/**/*.ts"],
                outDir: "www/dist",
            }
        },

        sass: {
            main: {
                options: {
                    style: 'expanded',
                    sourcemap: 'none'
                },
                files:
                    [{
                        expand: true,
                        cwd: 'src',
                        src: ['**/*.scss'],
                        dest: 'www/dist',
                        ext: '.css'
                    }]
            },
        },

        uglify: {
            libs: {
                files: {
                    "www/libs/libs.min.js": [
                        "node_modules/jquery/dist/jquery.js",
                        "node_modules/angular/angular.js",
                        "node_modules/@uirouter/angularjs/release/angular-ui-router.js",
                    ]
                }
            },
            prod: {
                // options: {
                //     mangle: false,
                //     beautify: true,
                // },
                files: {
                    "prod/min.js": [
                        "node_modules/jquery/dist/jquery.min.js",
                        "node_modules/angular/angular.min.js",
                        "node_modules/@uirouter/angularjs/release/angular-ui-router.min.js",
                        "www/dist/*.module.js",
                        "www/dist/**/*.module.js",
                        "www/templateCache/ngtemplates.js",
                        "www/dist/*.js",
                        "www/dist/**/*.js",
                        "!www/**/*.js.map",
                    ]
                }
            }
        },
        concat: {
            //options: { separator: ';', },
            libs: {
                src: [
                    "www/libs/libs.min.js",
                    "node_modules/socket.io-client/dist/socket.io.js",
                ],
                dest: 'www/libs/libs.min.js',
            },
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: 1
            },
            prod: {
                files: {
                    "prod/min.css": ["www/dist/sec-ripple/*.css", "www/**/*.css"]
                }
            },
        },
        ngtemplates: {
            main: {
                src: ['src/**/*.html'],
                dest: 'www/templateCache/ngtemplates.js',
                options: {
                    bootstrap: function (module, script) {
                        return '(function () { angular.module("sec")' +
                            '.run(["$templateCache", function($templateCache) {' + script +
                            '}]);})();';
                    }
                }
            },
        },
        watch: {
            scripts: {
                files: ['*.ts', '**/*.ts', '**/*.html', '**/*.scss'],
                tasks: ["sass", "ts", "ngtemplates"],
                // options: {
                //     spawn: false,
                // },
            },
        },
        copy: {
            prod: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['copy/index.html'],
                        dest: 'prod',
                        filter: 'isFile'
                    },
                ]
            }
        },
    });

    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask("default", ["sass", "ts", "ngtemplates"]);
    grunt.registerTask("libs", ["uglify:libs", "concat:libs"]);


    var production = ["sass", "ts", "ngtemplates", "uglify:prod", "cssmin:prod", "copy:prod"]
    grunt.registerTask("production", production);
    grunt.registerTask("p", production);

    grunt.registerTask("pp", ["uglify:prod", "cssmin:prod", "copy:prod"]);



};
