# Installation
* `npm init -y`
* `npm i -g live-server`
* `npm i --save angular @uirouter/angularjs angular-animate angular-animate angular-sanitize jquery`
* `npm i --save-dev grunt grunt-ts grunt-contrib-concat grunt-contrib-watch grunt-contrib-copy grunt-contrib-cssmin grunt-contrib-sass grunt-contrib-uglify grunt-contrib-watch grunt-angular-templates grunt-contrib-htmlmin grunt-ts-compiler @types/angular  @types/angular-ui-router @types/angular-animate @types/angular-sanitize @types/angular-mocks @types/node @types/jquery @types/socket.io @types/socket.io-client @types/socket.io-redis @types/socket.io.users typescript http-server`



# Must Ruby and Sass installed
* `gem install sass`
* `gem install compass` (4 windows)



# TypeSearch
* `https://microsoft.github.io/TypeSearch`



# Run
* `grunt watch` Or `node node_modules\http-server\bin\http-server`














