
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json"),

        htmlmin: {
            main: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    keepClosingSlash: true,
                    //conservativeCollapse: true,
                },
                files: {
                    "app/main/htmlmin/mainView.html": "app/main/views/mainView.html",

                    "app/web/htmlmin/aboutView.html": "app/web/views/aboutView.html",
                    "app/web/htmlmin/contactUsView.html": "app/web/views/contactUsView.html",
                    "app/web/htmlmin/helpView.html": "app/web/views/helpView.html",
                    "app/web/htmlmin/homeView.html": "app/web/views/homeView.html",
                    "app/web/htmlmin/loginView.html": "app/web/views/loginView.html",
                    "app/web/htmlmin/managementSelectionView.html": "app/web/views/managementSelectionView.html",
                    "app/web/htmlmin/masterView.html": "app/web/views/masterView.html",
                    "app/web/htmlmin/registerView.html": "app/web/views/registerView.html",
                    "app/web/htmlmin/validateRegistrationView.html": "app/web/views/validateRegistrationView.html",
                }
            },
            suppliers: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    keepClosingSlash: true,
                },
                files: {
                    "Areas/Suppliers/app/main/htmlmin/homeView.html": "Areas/Suppliers/app/main/views/homeView.html",
                    "Areas/Suppliers/app/main/htmlmin/mainView.html": "Areas/Suppliers/app/main/views/mainView.html",
                    "Areas/Suppliers/app/main/htmlmin/masterView.html": "Areas/Suppliers/app/main/views/masterView.html",
                    "Areas/Suppliers/app/main/htmlmin/addView.html": "Areas/Suppliers/app/main/views/addView.html",
                }
            }
        },
        ngtemplates: {
            main: {
                src: ['app/main/htmlmin/*.html', 'app/web/htmlmin/*.html'],
                dest: 'app/web/templateCache/ngtemplates.js',
                options: {
                    bootstrap: function (module, script) {
                        return '(function () { "use strict"; angular.module("appWeb").run(["$templateCache", ' +
                            'function($templateCache) {' + script + '}]);})();';
                    }
                }
            },
            suppliers: {
                src: ['app/main/htmlmin/*.html', 'Areas/Suppliers/app/main/htmlmin/*.html'],
                dest: 'Areas/Suppliers/app/main/templateCache/ngtemplates.js',
                options: {
                    bootstrap: function (module, script) {
                        return '\n\
                            \n(function () {\
                                \n"use strict";\
                                \nangular.module("appSuppliers")\n\
                                    \.run(["$templateCache", function($templateCache) {\n\
                                ' + script + '\
                                \n}]);\n\
                            \n})();';
                    }
                }
            }
        },
        uglify: {
            main: {
                files: {
                    "wwwroot/app.min.js": [
                        "Scripts/libs/jQuery/dist/jquery.min.js",
                        "Scripts/libs/angular/angular.min.js",
                        "Scripts/libs/angular-ui-router/release/angular-ui-router.min.js",
                        "Scripts/libs/angular-cookies/angular-cookies.min.js",
                        "Scripts/libs/angular-local-storage/dist/angular-local-storage.min.js",
                        "app/main/app.js",
                        "app/web/appWeb.js",
                        "app/web/templateCache/ngtemplates.js",
                        "app/**/*.js"
                    ]
                }
            },
            suppliers: {
                files: {
                    "Areas/Suppliers/wwwroot/app.min.js": [
                        "Scripts/libs/jQuery/dist/jquery.min.js",
                        "Scripts/libs/angular/angular.min.js",
                        "Scripts/libs/angular-ui-router/release/angular-ui-router.min.js",
                        "Scripts/libs/angular-cookies/angular-cookies.min.js",
                        "Scripts/libs/angular-local-storage/dist/angular-local-storage.min.js",
                        "app/main/app.js",
                        "Areas/Suppliers/app/main/suppliersApp.js",
                        "Areas/Suppliers/app/main/templateCache/ngtemplates.js",
                        "Areas/Suppliers/app/**/*.js"
                    ]
                }
            }
        },
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['Content/Images/*'],
                        dest: 'wwwroot/Images',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: 'Content/Fonts/',
                        src: ['**'],
                        dest: 'wwwroot/Fonts'
                    },

                ]
            }
        },
        sass: {
            main: {
                options: {
                    style: 'expanded',
                    sourcemap: 'none'
                },
                files:
                    [{
                        expand: true,
                        cwd: 'Content/SCSS/',
                        src: ['*.scss'],
                        dest: 'Content/CSS',
                        ext: '.css'
                    }, {
                        expand: true,
                        cwd: 'Content/SCSS/Models',
                        src: ['*.scss'],
                        dest: 'Content/CSS',
                        ext: '.css'
                    }, {
                        expand: true,
                        cwd: 'Content/SCSS/Pages',
                        src: ['*.scss'],
                        dest: 'Content/CSS',
                        ext: '.css'
                    }, {
                        expand: true,
                        cwd: 'Content/CSS/Comcated/',
                        src: ['*.scss'],
                        dest: 'Content/CSS/Comcated/',
                        ext: '.css'
                    }]
            },
            //suppliers: {
            //    files:
            //        [{
            //            expand: true,
            //            cwd: 'Areas/Suppliers/Content/SCSS/',
            //            src: ['*.scss'],
            //            dest: 'Areas/Suppliers/Content/CSS',
            //            ext: '.css'
            //        }, {
            //            expand: true,
            //            cwd: 'Areas/Suppliers/Content/SCSS/Models',
            //            src: ['*.scss'],
            //            dest: 'Areas/Suppliers/Content/CSS',
            //            ext: '.css'
            //        }, {
            //            expand: true,
            //            cwd: 'Areas/Suppliers/Content/SCSS/Pages',
            //            src: ['*.scss'],
            //            dest: 'Areas/Suppliers/Content/CSS',
            //            ext: '.css'
            //        }, {
            //            expand: true,
            //            cwd: 'Areas/Suppliers/Content/CSS/Comcated/',
            //            src: ['*.scss'],
            //            dest: 'Areas/Suppliers/Content/CSS/Comcated/',
            //            ext: '.css'
            //        }]
            //}
        },
        concat: {
            //options: { separator: ';', },
            main: {
                src: [
                    "Content/CSS/reset.css",
                    "Content/CSS/UI.css",
                    "Content/CSS/fonts.css",
                    "Content/CSS/main.css",
                    "Content/CSS/drawer.css",
                    "Content/CSS/window.css",
                    "Content/CSS/input-view.css",
                    "Content/CSS/sec-checkbox.css",
                    "Content/CSS/logo.css",
                    "Content/CSS/managementSelection.css", 
                    "Content/CSS/sec-full-screen.css", 
                    "Content/CSS/sec-logout.css"
                ],
                dest: 'Content/CSS/Comcated/all_CSS_files.css',
            },
            //suppliers: {
            //    src: [
            //        "Areas/Suppliers/Content/CSS/reset.css",
            //        "Areas/Suppliers/Content/CSS/UI.css",
            //        "Areas/Suppliers/Content/CSS/fonts.css",
            //        "Areas/Suppliers/Content/CSS/main.css",
            //        "Areas/Suppliers/Content/CSS/drawer.css",
            //        "Areas/Suppliers/Content/CSS/window.css",
            //        "Areas/Suppliers/Content/CSS/input-view.css",
            //        "Areas/Suppliers/Content/CSS/sec-checkbox.css",
            //        "Areas/Suppliers/Content/CSS/logo.css",
            //        "Areas/Suppliers/Content/CSS/managementSelection.css"
            //    ],
            //    dest: 'Areas/Suppliers/Content/CSS/Comcated/all_CSS_files.css',
            //}
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: 1
            },
            main: {
                files: {
                    "wwwroot/css/app.min.css": ["Content/CSS/Comcated/all_CSS_files.css"]
                }
            },
            //suppliers: {
            //    files: {
            //        "Areas/Suppliers/wwwroot/css/app.min.css": ["Areas/Suppliers/Content/CSS/Comcated/all_CSS_files.css"]
            //    }
            //}
        }
        
    });

    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    

    // main tasks
    grunt.registerTask("default_main", ["htmlmin:main", "ngtemplates:main", "copy:main", "uglify:main", "sass:main", "concat:main", "cssmin:main"]);

    grunt.registerTask("main_sass", ["sass:main", "concat:main", "cssmin:main"]);

    grunt.registerTask("main_copy_images_and_fonts", ["copy:main"]);

    grunt.registerTask("main_ngt", ["htmlmin:main", "ngtemplates:main"]);

    // end main tasks



    // suppliers tasks
    grunt.registerTask("default_suppliers", ["htmlmin:suppliers", "ngtemplates:suppliers", "uglify:suppliers", "sass", "concat", "cssmin"]);

    grunt.registerTask("suppliers_sass", ["sass", "concat", "cssmin"]);

    grunt.registerTask("suppliers_ngt", ["htmlmin:suppliers", "ngtemplates:suppliers"]);
    // end suppliers tasks



};


//watch: {
//    sass: {
//        files: ["Content/**/*.scss"],
//        task: ["sass"]
//    },
//    scripts: {
//        files: ["app/**/*.js", "Content/**/*.css"],
//        task: ["uglify"]
//    },
//    cssmin: {
//        files: {
//            "wwwroot/css/app.min.css": ["Content/**/*.css"]
//        }
//    },
//},


//copy: {
//    main: {
//        files: [
//            {
//                expand: true,
//                flatten: true,
//                src: ['app/*.js'],
//                dest: 'scriptsDevs',
//                filter: 'isFile'
//            },
//            {
//                expand: true,
//                flatten: true,
//                src: ['app/**/*.js'],
//                dest: 'scriptsDevs',
//                filter: 'isFile'
//            },
//            {
//                expand: true, src: ['app/**/*.html'],
//                dest: 'templates',
//                filter: 'isFile'
//            },
//        ]
//    }
//},